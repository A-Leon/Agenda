package com.example.agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    final ArrayList<Contacto> contactos= new ArrayList<Contacto>();
    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    Contacto saveContact;
    int savedIndex;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre= (EditText) findViewById(R.id.txtNombre);
        edtTelefono= (EditText) findViewById(R.id.txtTel1);
        edtTelefono2= (EditText) findViewById(R.id.txtTel2);
        edtDireccion = (EditText) findViewById(R.id.txtDomicilio);
        edtNotas =(EditText) findViewById(R.id.txtNota);
        cbxFavorito = (CheckBox)findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btnSalir = (Button) findViewById(R.id.btnSalir);


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edtNombre.getText().toString().equals("")||
                edtDireccion.getText().toString().equals("")||
                edtTelefono.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, R.string.mensaje,
                            Toast.LENGTH_SHORT).show();
                }else{

                    Contacto nCotacto = new Contacto();
                    int index = contactos.size();
                    if(saveContact !=null)
                    {
                        contactos.remove(savedIndex);
                        nCotacto=saveContact;
                        index= savedIndex;


                    }
                    nCotacto.setNombre(edtNombre.getText().toString());
                    nCotacto.setTelefono1(edtTelefono.getText().toString());
                    nCotacto.setTelefono2(edtTelefono2.getText().toString());
                    nCotacto.setDomicilio(edtDireccion.getText().toString());
                    nCotacto.setNotas(edtNotas.getText().toString());
                    nCotacto.setFavorito(cbxFavorito.isChecked());
                    contactos.add(savedIndex,nCotacto);
                    Toast.makeText(MainActivity.this,R.string.mensaje, Toast.LENGTH_SHORT).show();
                    saveContact = null;
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                Bundle bObject = new Bundle();
                bObject.putSerializable("contactos", contactos);
                i.putExtras(bObject);
                startActivityForResult(i,0);
            }
        });


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    protected  void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode, resultCode, intent);
        if(intent != null){
            Bundle oBundle = intent.getExtras();
            saveContact = (Contacto) oBundle.getSerializable("contacto");
            savedIndex = oBundle.getInt("index");
            edtNombre.setText(saveContact.getNombre());
            edtTelefono.setText(saveContact.getTelefono1());
            edtTelefono2.setText(saveContact.getTelefono2());
            edtDireccion.setText(saveContact.getDomicilio());
            edtNotas.setText(saveContact.getNotas());
        }else{
            limpiar();

        }
    }

    public void limpiar()
    {
        edtNombre.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtDireccion.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);

    }

}
