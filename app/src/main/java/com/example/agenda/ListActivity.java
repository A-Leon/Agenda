package com.example.agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;
import java.util.ArrayList;



public class ListActivity extends AppCompatActivity {



    TableLayout tblLista;
    ArrayList<Contacto> contactos;
    private ArrayList<Contacto> filther;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        tblLista =(TableLayout) findViewById(R.id.tblLista);
        Bundle bundleObject = getIntent().getExtras();
        contactos= (ArrayList<Contacto>) bundleObject.getSerializable("contactos");
        filther= contactos;
        Button btnNuevo =( Button)findViewById(R.id.btnNuevo);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });

        cargarContactos();
    }


    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                buscar(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


    private void buscar(String s){
        ArrayList<Contacto> list = new ArrayList<>();
        for (int x=0;x<filther.size(); x++){
            if(filther.get(x).getNombre().contains(s))
                list.add(filther.get(x));
        }
        contactos = list;
        tblLista.removeAllViews();
        cargarContactos();
    }



    public void cargarContactos(){

        for (int x=0; x<contactos.size(); x++)
        {
            Contacto c= contactos.get(x);
            TableRow nRow = new TableRow(ListActivity.this);

            final TextView nText = new TextView(ListActivity.this);
            nText.setText(c.getNombre());

            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            nText.setTextColor((c.isFavorito()) ? Color.BLUE: Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(ListActivity.this);
            nButton.setText(R.string.accver);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            nButton.setTextColor(Color.BLACK);

            Button cButton = new Button(ListActivity.this);
            cButton.setText("Borrar");
            cButton.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            cButton.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Contacto c= (Contacto) view.getTag(R.string.contacto_g);
                    Intent i = new Intent();
                    Bundle Obundle = new Bundle ();
                    Obundle.putSerializable("contacto", c);
                    Obundle.putInt("index", Integer.valueOf(view.getTag(R.string.contacto_g_index).toString()));
                    i.putExtras(Obundle);
                    setResult(RESULT_OK, i);
                    finish();



                }
            });

            cButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String c= nText.getText().toString();
                    for (int x=0; x<filther.size(); x++)
                    {

                        if (c==filther.get(x).getNombre()){
                            filther.remove(x);
                            break;
                        }
                    }
                    contactos = filther;
                    tblLista.removeAllViews();
                    cargarContactos();


                }
            });

            nButton.setTag(R.string.contacto_g, c);
            nButton.setTag(R.string.contacto_g_index, x);
            cButton.setTag(R.string.contacto_g, c);
            cButton.setTag(R.string.contacto_g_index, x);
            nRow.addView(nButton);
            nRow.addView(cButton);
            tblLista.addView(nRow);


        }
    }

    private void eliminar(long id)
    {
        for(int x = 0; x <filther.size(); x++)
        {
            if(filther.get(x).getID() == id)
            {
                filther.remove(x);
                break;
            }
        }
        contactos = filther;
        tblLista.removeAllViews();
        cargarContactos();

    }



}
