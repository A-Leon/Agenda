package com.example.agenda;

import java.io.Serializable;

public class Contacto implements Serializable {

    private long ID;
    private String nombre;
    private String telefono1;
    private String telefono2;
    private String domicilio;
    private  String notas;
    private boolean favorito;

    public Contacto(long ID, String nombre, String telefono1, String telefono2, String domicilio, String notas, boolean favorito) {
        this.ID = ID;
        this.nombre = nombre;
        this.telefono1 = telefono1;
        this.telefono2 = telefono2;
        this.domicilio = domicilio;
        this.notas = notas;
        this.favorito = favorito;
    }

    public Contacto() {
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public boolean isFavorito() {
        return favorito;
    }

    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }
}
